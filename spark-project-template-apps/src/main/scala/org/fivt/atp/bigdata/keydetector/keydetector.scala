package org.fivt.atp.bigdata.keydetector

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.types.StringType

object KeyDetectorMain {

  def keyCheck(word: String): Boolean = {
    val keyWords = List("in", "out", "on", "with")
    keyWords.contains(word)
  }

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .getOrCreate()

    val kafkaBrokers = "mipt-node04.atp-fivt.org:9092"
    val inputTopic = "simple-input-user2020002"
    val inputStream = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("subscribe", inputTopic)
      .option("startingOffsets", "latest")
      .load()

    val kafkaValueColumn = "value"
    val deserializedColumn = "deserialized_value"
    val deserializedStream = inputStream
      .select(col(kafkaValueColumn) cast StringType
        as deserializedColumn)
    
    val transform = (string: String) => 
      string.replaceAll("""[\p{Punct}]""", "")
            .toLowerCase()
            .split(' ')

    val transformUDF = udf(transform) 

    val transformedColumn = "transformed_value"
    val outputStream = deserializedStream
      .select(explode(transformUDF(col(deserializedColumn))) as transformedColumn)
      .filter(word => keyCheck(word.get(0).toString))
      .select(col(transformedColumn) as kafkaValueColumn)

    val outputTopic = "simple-output-user2020002"
    val checkpointDir = "checkpoints/keydetector"
    val query = outputStream.writeStream
      .queryName(this.getClass.getSimpleName)
      .outputMode(OutputMode.Append())
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("topic", outputTopic)
      .option("checkpointLocation", checkpointDir)
      .start()
    query.awaitTermination()

  }
}
