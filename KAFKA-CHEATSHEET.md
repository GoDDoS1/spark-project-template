# Kafka Cheatsheet

Useful commands with Kafka console utilities

List Kafka topics
```shell script
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --list
```

Create topics
```shell script
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --create --replication-factor 1 --partitions 4 --topic simple-input-$USER
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --create --replication-factor 1 --partitions 4 --topic simple-output-$USER

```

Describe topics
```shell script
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --describe --topic simple-input-$USER
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --describe --topic simple-output-$USER
```

Consume from a topic
```shell script
kafka-console-consumer --bootstrap-server mipt-node04.atp-fivt.org:9092 --from-beginning --topic simple-output-$USER
```

Produce to a topic
```shell script
kafka-console-producer --broker-list mipt-node04.atp-fivt.org:9092 --topic simple-input-$USER
```

Delete topics
```shell script
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --delete --topic simple-input-$USER
kafka-topics --zookeeper mipt-master.atp-fivt.org:2181 --delete --topic simple-output-$USER
```
